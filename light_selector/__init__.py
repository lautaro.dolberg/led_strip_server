__version__ = '0.1.0'
from flask import Flask, render_template, url_for
import time

def create_app() -> Flask:
    """
    Creates an application instance to run
    :return: A Flask object
    """
    app = Flask(__name__, static_folder='site', static_url_path='/')
 
    from . import blueprints
    blueprints.init_app(app)
    @app.route('/api/time')
    def get_current_time():
        return {'time': time.time()}
    
    @app.route('/')
    def index():
        print(app.static_folder)
        return app.send_static_file('index.html')

    return app