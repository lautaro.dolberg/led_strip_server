
# light_selector/blueprints/__init__.py
from flask import Flask


def init_app(app: Flask):
    from .root import root_blueprint
    from .lights import light_blueprint
    app.register_blueprint(root_blueprint)
    app.register_blueprint(light_blueprint, url_prefix='/lights')