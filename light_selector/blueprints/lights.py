from flask import Blueprint, render_template, g, jsonify
from flask import current_app
import paho.mqtt.publish as publish
import itertools
from collections import defaultdict
from flask_mqtt import Mqtt
import datetime

mqtt = Mqtt()


clients = defaultdict(int)
states = defaultdict(bool)

@mqtt.on_connect()
def handle_connect(client, userdata, flags, rc):
    mqtt.subscribe("esp/+/state")


@mqtt.on_message()
def handle_mqtt_message(client, userdata, message):
    data = dict(topic=message.topic, payload=message.payload.decode())
    client = data["topic"].split("/")[1]
    clients[client] = int(datetime.datetime.now().timestamp())
    # print(f"Registered {client}")


# @mqtt.on_log()
# def handle_logging(client, userdata, level, buf):
#     print(level, buf)

default_dev_ids = [
    "5f4b10cae4a13222250bd12ed0138c84",
    "453dd89038f38ef4829c251ab4e1a20f",
    "f82258f0b3ec6415cbb4ecc3d34decf6",
]
light_state = False


def toggle():
    global light_state
    light_state = not light_state


default_color = "FF9329"


light_blueprint = Blueprint(
    "lights_blueprint",
    __name__,
    static_folder="static",
    template_folder="templates/lights",
)

@light_blueprint.route("/devices")
def get_devices():
    return jsonify(clients)

@light_blueprint.route("/")
def index():
    return render_template("index.html")


@light_blueprint.route("/state")
def get_state():
    return jsonify({"state": light_state})


@light_blueprint.route("/toggle", defaults={"device": None})
@light_blueprint.route("/toggle/<device>")
def set_toggle(device):
    if device is not None and device in clients.keys():
        col = default_color if states[device] else "000000"
        publish.single(f"esp/{device}/lights", f"{col}", hostname="lightpi")
        states[device] = not states[device]
        return jsonify({"state": states[device]})
    else:
        for device_id in clients.keys():
            col = default_color if light_state else "000000"
            publish.single(f"esp/{device_id}/lights", f"{col}", hostname="lightpi")
        toggle()
    return jsonify({"state": light_state})


@light_blueprint.route("/set", defaults={"color": default_color})
@light_blueprint.route("/set/<color>")
def set_color(color=default_color):
    for device_id in clients.keys():
        publish.single(f"esp/{device_id}/lights", f"{color}", hostname="lightpi")
    light_state = True
    return jsonify({"state": light_state})


# @light_blueprint.route('/set/<device_id>/<color>')
# def set_color(device_id, color):
#     publish.single(f"esp/{device_id}/lights", f"{color}", hostname="localhost")
#     return  {'message': f"setting {device_id}: {color}"}


@light_blueprint.record
def mqtt_register(obj):
    # print(obj)
    obj.app.config["MQTT_BROKER_URL"] = "lightpi"  # use the free broker from HIVEMQ
    obj.app.config["MQTT_BROKER_PORT"] = 1883  # default port for non-tls connection
    obj.app.config[
        "MQTT_USERNAME"
    ] = ""  # set the username here if you need authentication for the broker
    obj.app.config[
        "MQTT_PASSWORD"
    ] = ""  # set the password here if the broker demands authentication
    obj.app.config[
        "MQTT_KEEPALIVE"
    ] = 5  # set the time interval for sending a ping to the broker to 5 seconds
    obj.app.config["MQTT_TLS_ENABLED"] = False
    mqtt.init_app(obj.app)
